import urllib.request as request
import json

src="https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=fba6ad08-ec66-4d9b-8a71-4c76b36704f7"
with request.urlopen(src) as response:
    data=json.load(response)
print(data)

# 取得資料內容
list=data["result"]["results"]
print(list)
with open("open-data.txt", mode="w", encoding="utf-8") as file:
    for x in list:
        file.write(x["ADDRESS"]+"\n")