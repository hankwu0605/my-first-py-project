import random

data=[1,3,4,6,7,8]
# 隨機選取
print(random.choice(data))
print(random.sample(data,3))

# 隨機調換順序
random.shuffle(data)
print(data)

# 隨機亂數
print(random.random())
print(random.uniform(1.0,5.0))

# 常態分配
print(random.normalvariate(100,10))

import statistics as stat

# 平均數
print(stat.mean(data))

# 中位數
print(stat.median(data))