# String
"中文"

# boolean
True
False

# List
list=[3,4,5]
len=len(list)

# Tuple
(3,4,5)

# Set
{3,4,5}

# Dictionary (Key-Value)
{"Apple":"蘋果","Banana":"香蕉"}