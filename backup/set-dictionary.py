# 集合的運算
s1={1,2,3}
s2={2,3,4,5}

1 in s1
4 not in s1

# 交集
s1&s2

# 聯集
s1|s2

# 差集
s1-s2

# 反交集
s1^s2


# Dictionary
dic={"Apple":"蘋果","Banana":"香蕉"}
dic["Apple"]
"Apple" in dic
del dic["Apple"]

dic={x:x*2 for x in [3,4,5]}
print(dic)