# file=open("data.txt", mode="w", encoding="utf-8")
# file.write("HelloFile")
# file.close

with open("data.txt", mode="w", encoding="utf-8") as file:
    file.write("5\n3")

sum=0
with open("data.txt", mode="r", encoding="utf-8") as file:
    print(file)
    for line in file:
        print(line)
        sum+=int(line)
print(sum)

import json
with open("config.json", mode="r") as file:
    data=json.load(file)
print(data)
data["name"]="NewName"
with open("config.json", mode="w") as file:
    json.dump(data,file)