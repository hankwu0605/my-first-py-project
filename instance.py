class Point:
    def __init__(self,x,y):
        super().__init__()
        self.x=x
        self.y=y
    def show(self):
        print(self.x, self.y)

p=Point(3,4)
print(p.x+p.y)
p.show()

class File:
    def __init__(self,name):
        super().__init__()
        self.name=name
        self.file=None
    def open(self):
        self.file=open(self.name, mode="r", encoding="utf-8")
    def read(self):
        return self.file.read()

f1=File("instance-test1.txt")
f1.open()
print(f1.read())